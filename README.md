# plugin-universis-api-user-activity

An example project  which introduces a plugin of universis api server for logging user activity like user logins etc

`plugin-universis-api-user-activity` plugin extends api server for logging user activity:

- Adds `UserLoginActivity` data model for storing user sessions.

        {
            "$schema": "https://themost-framework.github.io/themost/models/2018/2/schema.json",
            "@id": "https://universis.io/schemas/UserLoginAction",
            "name": "UserLoginAction",
            "description": "The act of requesting something with a specific token",
            "title": "UserLoginActions",
            "abstract": false,
            "sealed": false,
            "version": "1.5"
            ...
        }
- Extends `@universis/api#User` class by adding a new method `User.getLoginActivity()` in order to get user sessions.

        getLoginActivity() {
                return this.context.model('UserLoginAction').where('owner/name').equal(this.context.user.name).prepare();
            }
- Registers `User.getLoginActivity()` as an entity function for serving data through /api/users/me/loginActivity endpoint

        @EdmMapping.func("loginActivity", EdmType.CollectionOf('UserLoginAction'))
            getLoginActivity() {
                return this.context.model('UserLoginAction').where('owner/name').equal(this.context.user.name).prepare();
            }

### Development

##### Clone universis-api server, install dependencies and build

        git clone https://gitlab.com/universis/universis-api.git
        cd universis-api
        npm i
        npm run build

#####  Clone project and install dependencies

       git clone https://gitlab.com/universis/plugin-universis-api-user-activity.git
       cd plugin-universis-api-user-activity
       npm i
       
##### Install universis-api as development dependency (for testing)

       npm i ../universis-api --save-dev
       
##### Test plugin

        npm test
