import {EdmMapping, EdmType} from "@themost/data/odata";

/**
 * @augments {DataObject}
 */
export class UserActivityUser {
    /**
     * Gets an instance of DataQueryable class for getting user login actions. This method is going to used by universis api service router
     * @returns {DataQueryable}
     */
    @EdmMapping.func("loginActivity", EdmType.CollectionOf('UserLoginAction'))
    getLoginActivity() {
        return this.context.model('UserLoginAction').where('owner/name').equal(this.context.user.name).prepare();
    }
}
