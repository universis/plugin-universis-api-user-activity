import {ApplicationService} from '@themost/common';
import UserLoginActionModel from './config/models/UserLoginAction';
import {UserActivityUser} from './UserActivityUser';
import {ModelClassLoaderStrategy, ODataModelBuilder} from "@themost/data";
import {DataConfigurationStrategy} from '@themost/data';
import {ApplicationServiceRouter} from '@themost/express';

export class UserActivityService extends ApplicationService {
    /**
     * @param {IApplication} app
     */
    constructor(app) {
        super(app);
        this.install();
        // extend application service router
    }

    install() {
        /**
         * get data configuration
         * @type {DataConfigurationStrategy}
         */
        const configuration = this.getApplication().getConfiguration().getStrategy(DataConfigurationStrategy);
        // set UserLoginAction model definition
        // this operation will include UserLoginAction model to the collection of api server data models
        configuration.setModelDefinition(UserLoginActionModel);
        /**
         * get ODataModelBuilder service
         * @type {ODataModelBuilder|*}
         */
        const builder = this.getApplication().getStrategy(ODataModelBuilder);
        // add entity set for UserLoginAction model
        builder.addEntity(UserLoginActionModel.name);
        // extend user model class
        const userModelDefinition = configuration.getModelDefinition('User');
        /**
         * get model class loader
         * @type {ModelClassLoaderStrategy}
         */
        const classLoader = this.getApplication().getConfiguration().getStrategy(ModelClassLoaderStrategy);
        /**
         * get user data model class
         * @type {*}
         */
        const User = classLoader.resolve(userModelDefinition);
        // add some method to User.prototype
        User.prototype.getLoginActivity = UserActivityUser.prototype.getLoginActivity;
        // forcibly set DataObjectClass property of model User
        userModelDefinition.DataObjectClass = User;
        // update model
        configuration.setModelDefinition(userModelDefinition);
        // refresh Users entity set
        (function refreshEntitySet(entityType, entitySet) {
            builder.removeEntitySet(entitySet);
            builder.addEntitySet(entityType, entitySet);
        })('User','Users');

        // extend serviceRouter
        // noinspection JSValidateTypes
        /**
         * @type {ApplicationServiceRouter}
         */
        const applicationServiceRouter = this.getApplication().getService(ApplicationServiceRouter);
        if (applicationServiceRouter) {
            //const serviceRouter = applicationServiceRouter.getServiceRouter();
            applicationServiceRouter.serviceRouter.use((req, res, next) => {
                // place code here for saving login activity
               return next();
            });
            // move route up
            const index = applicationServiceRouter.serviceRouter.stack.length - 1;
            const route = applicationServiceRouter.serviceRouter.stack[index];
            applicationServiceRouter.serviceRouter.stack.splice(index, 1);
            applicationServiceRouter.serviceRouter.stack.unshift(route);
            console.log(applicationServiceRouter.serviceRouter);
        }

    }

    uninstall() {
        // place your code here
    }

}
