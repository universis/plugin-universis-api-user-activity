import app from '@universis/api/dist/server/app';
import {ExpressDataApplication} from '@themost/express';
import {UserActivityService} from "./UserActivityService";
import {DataCacheStrategy, DataConfigurationStrategy} from '@themost/data';
import {RandomUtils, TextUtils} from '@themost/common';
import request from 'supertest';
import {OAuth2ClientService} from "@universis/api/dist/server/services/oauth2-client-service";

const DEFAULT_ADAPTER = {
    name: 'memory-db',
    invariantName: 'memory',
    default: true,
    options: {
        name: 'memory-db'
    }
};
const DEFAULT_ADAPTER_TYPE = {
    name: 'Memory Data Adapter',
    invariantName: 'memory',
    type: '@themost/mem'
};
describe('UserActivityService', () => {
    /**
     * @type {ExpressDataApplication}
     */
    let application;
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(() => {
        /**
         * @type {ExpressDataApplication}
         */
        application =  app.get(ExpressDataApplication.name);
        // set default adapter for testing
        const adapterTypes = application.getConfiguration().getSourceAt('adapterTypes');
        const findAdapterType = adapterTypes.find( x => {
            return x.invariantName === DEFAULT_ADAPTER_TYPE.invariantName;
        });
        if (findAdapterType == null) {
            adapterTypes.push(DEFAULT_ADAPTER_TYPE);
        }
        application.getConfiguration().setSourceAt('adapterTypes', adapterTypes);
        // add memory adapter type
        const adapters = application.getConfiguration().getSourceAt('adapters');
        const findAdapter = adapters.find( x => {
           return x.name === DEFAULT_ADAPTER.name;
        });
        // remove default adapter if any
        adapters.forEach( x => {
            x.default = false;
        });
        // set default adapter
        if (findAdapter == null) {
            adapters.push(DEFAULT_ADAPTER);
        }
        let model = application.getConfiguration().getStrategy(DataConfigurationStrategy).getModelDefinition('Permission');
        model.caching = 'none';
        application.getConfiguration().getStrategy(DataConfigurationStrategy).setModelDefinition(model);
        application.getConfiguration().setSourceAt('adapters', adapters);
        application.getConfiguration().useStrategy(DataConfigurationStrategy, DataConfigurationStrategy);

    });

    beforeEach(async () => {
        context = application.createContext();
        // add test users
        await context.model('User').silent().save([
            {
                name: 'anonymous',
                groups: [
                    {
                        alternateName: 'Guests'
                    }
                ]
            },
            {
                name: 'user1@example.com',
                groups: [
                    {
                        alternateName: 'Users'
                    }
                ]
            },
            {
                name: 'user2@example.com',
                groups: [
                    {
                        alternateName: 'Users'
                    }
                ]
            }
        ]);
    });

    afterEach(done => {
        // important: clear cache after each test
       const configuration = context.getConfiguration();
       if (configuration.hasOwnProperty('cache')) {
           delete  configuration.cache;
       }
       context.finalize(() => {
           return done();
       });
    });


    it('should create instance', () => {
        const service  = new UserActivityService(application);
        expect(service).toBeTruthy();
    });

    it('should install service', () => {
        /**
         * @type {DataConfigurationStrategy}
         */
        const dataConfiguration = application.getConfiguration().getStrategy(DataConfigurationStrategy);
        // validate service
        expect(application.getService(UserActivityService)).toBeFalsy();
        // expect UserLoginAction to be undefined
        let model = dataConfiguration.getModelDefinition('UserLoginAction');
        expect(model).toBeTruthy();
        // use service
        application.useService(UserActivityService);
        //validate service instance
        expect(application.getService(UserActivityService)).toBeTruthy();
        // validate model definition
        model = dataConfiguration.getModelDefinition('UserLoginAction');
        expect(model).toBeTruthy();
    });

    it('should use UserLoginAction.insert()', async () => {
        if (application.hasService(UserActivityService) === false) {
            application.useService(UserActivityService)
        }
        const user = await context.model('User').where('name').equal('user1@example.com')
            .silent().getItem();
        // set context user
        context.user = {
            name: user.name,
            authenticationScope: 'students',
            authenticationToken: RandomUtils.randomChars(48),
            authenticationProviderKey:  user.id,
            authenticationType: 'basic'
        };
        // add user login action
        const code =  TextUtils.toMD5(context.user.authenticationToken);
        await context.model('UserLoginAction').silent().save({
            code: code
        });
        const item = await context.model('UserLoginAction')
            .where('code').equal(code)
            .silent().getItem();
        expect(item).toBeTruthy();
        expect(item.owner).toBe(user.id);
    });

    it('should use UserLoginAction.remove()', async () => {
        if (application.hasService(UserActivityService) === false) {
            application.useService(UserActivityService)
        }
        // get user for testing
        const user = await context.model('User').where('name').equal('user1@example.com')
            .silent().getItem();
        context.user = {
            name: user.name,
            authenticationScope: 'students',
            authenticationToken: RandomUtils.randomChars(48),
            authenticationProviderKey:  user.id,
            authenticationType: 'basic'
        };
        // add user login action
        const code =  TextUtils.toMD5(context.user.authenticationToken);
        await context.model('UserLoginAction').silent().save({
            code: code
        });
        try {
            await context.model('UserLoginAction').remove({
                code: code
            });
        }
        catch(err) {
            expect(err).toBeTruthy();
        }
        await context.model('UserLoginAction').silent().remove({
            code: code
        });
    });

    it('should use UserLoginAction.getItems()', async () => {
        if (application.hasService(UserActivityService) === false) {
            application.useService(UserActivityService)
        }
        // get user for testing
        const user = await context.model('User').where('name').equal('user1@example.com')
            .silent().getItem();
        context.user = {
            name: user.name,
            authenticationScope: 'students',
            authenticationToken: RandomUtils.randomChars(48),
            authenticationProviderKey:  user.id,
            authenticationType: 'basic'
        };
        // add user login actions for new user
        await context.model('UserLoginAction').silent().save([
            {
                code: TextUtils.toMD5(RandomUtils.randomChars(48)),
                owner: {
                    name: 'user2@example.com'
                }
            }, {
                code: TextUtils.toMD5(RandomUtils.randomChars(48)),
                owner: {
                    name: 'user2@example.com'
                }
            }]);
        // add user login action
        const code =  TextUtils.toMD5(context.user.authenticationToken);
        await context.model('UserLoginAction').silent().save({
            code: code
        });
        // get user1 login actions
        const items = await context.model('UserLoginAction').asQueryable().getItems();
        expect(items.length).toBeGreaterThan(0);
        items.forEach( item => {
            expect(item.owner).toBe(user.id);
        });

    });

    it('should GET 401 /users/me/loginActivity', async () => {
        let response = await request(app)
            .get('/api/users/me/loginActivity')
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        expect(response.status).toBe(401);
    });

    fit('should GET /users/me/loginActivity', async () => {
        // use UserActivityService service
        if (application.hasService(UserActivityService) === false) {
            application.useService(UserActivityService)
        }
        const user = {
            name: 'user1@example.com',
            authenticationScope: 'students',
            authenticationToken: RandomUtils.randomChars(48),
            authenticationType: 'Bearer'
        };
        // spy on OAuth2ClientService.getTokenInfo()
        let client = application.getStrategy(OAuth2ClientService);
        spyOn(client, 'getTokenInfo').and.returnValue(new Promise(resolve => {
            return resolve({
                active: true,
                username: user.name,
                scope: 'students'
            });
        }));
        // request data
        let response = await request(app)
            .get('/api/users/me/loginActivity')
            .set('Authorization', `Bearer ${user.authenticationToken}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        expect(response.status).toBe(200);
    });

});
